# Syslog-Ng server (by David) plus ComposeUI (by francescou)

## **IMPORTANT**: put it in the main projects folder, like in the next example:

`~/docker-compose-projects
        |
        -syslogng_plus_composeui/
        |   |
        |   -docker-compose.yml
        |   -docker-compose-projects (create this symbolic link pointing previous folder)
        |
        -project-a
        |   |
        |   -docker-compose.yml
        -project-b
        |   |
        |   -docker-compose.yml
`


### Inside the project folder there is a link, pointing the parent folder. The ComposeUi need to find other dockerfiles

### In this way you can control this environment too, for stopping purpose

## UP

`# sudo docker-compose up -d`

Point your browser to http://localhost:1500

This project ships the syslog-ng server configured to listen on **two ports**:

0.0.0.0:`1514` - > SINGLE plain log file for all

0.0.0.0:`1518` - > MULTIPLE separete files grouped by docker hosts daemon subfolder and error level in the file name

Run latest docker daemon and compose for more readable log entries ( daemon > 1.8 && compose > 1.4 ) 

The syslog-ng container writes persistent data log outside the container. Look inside the project folder

With the UI you can check which container is putting log in the syslog server

## Credits for compose UI goes to
## [francescou](https://hub.docker.com/r/francescou/docker-compose-ui/)

I know I have the approval to include it in this project and i **thank you!** ;)
